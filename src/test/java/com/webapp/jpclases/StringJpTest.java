package com.webapp.jpclases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class StringJpTest {

	String a = "juan";
	String b = "pablo";
		
	@Test
	public void getConcatOfString() {
		StringsJp stringJpTest = new StringsJp();
		assertEquals("juan pablo debe ser juanpablo", "juanpablo", stringJpTest.concatenateString(a, b));
		assertEquals("alvisc colmenares debe ser alviscolmenares", "alvisccolmenares", stringJpTest.concatenateString("alvisc", "colmenares"));
		assertNotEquals("a b no debe ser ac", "ac", stringJpTest.concatenateString(a, b));	
	}

	@Test
	public void getfirstCharacterOfString() {
		StringsJp stringJpTest = new StringsJp();
		assertEquals("juan debe ser j", "j", stringJpTest.getFirstCharacter(a));
		assertEquals("pablo debe ser p", "p", stringJpTest.getFirstCharacter(b));
		assertNotEquals("juan no debe ser x", "x", stringJpTest.getFirstCharacter(a));		
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void getExceptionOfString() {
		StringsJp stringJpTest = new StringsJp();
		assertEquals("juan debe ser j", "j", stringJpTest.getFirstCharacter(""));		
	}

}
