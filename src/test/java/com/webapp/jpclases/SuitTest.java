package com.webapp.jpclases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
                MathJpTest.class,
                StringJpTest.class })

public class SuitTest {

}
