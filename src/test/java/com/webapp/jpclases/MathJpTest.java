package com.webapp.jpclases;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MathJpTest {

	@Test
	public void multiplicationOfZeroIntegersShouldReturnZero() {
		MathJp mathJpTest = new MathJp();
		assertEquals("10 * 0 debe ser 0", 0, mathJpTest.multipyTwoNumbers(10, 0));
		assertEquals("0 * 10 debe ser 0", 0, mathJpTest.multipyTwoNumbers(0, 10));
		assertEquals("0 * 0 debe ser 0", 0, mathJpTest.multipyTwoNumbers(0, 0));
		
	}

}
