package testwitheasymock;
import org.junit.*;

import com.webapp.controller.zk.utils.User;

public class UserTest {
	
	 private User classUnderTest;
	 
	 private Collaborator mock;

	  @Before
	  public void setUp() {
	    classUnderTest = new User();
//	    classUnderTest.setListener(mock);
	  }
//
	  @Test
	  public void testRemoveNonExistingDocument() {
	    // This call should not lead to any notification
	    // of the Mock Object:
//	    classUnderTest.removeDocument("Does not exist");
	  }
	  
	  public interface Collaborator {
		    void documentAdded(String title);
		}

}
