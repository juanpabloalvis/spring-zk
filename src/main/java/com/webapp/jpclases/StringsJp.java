package com.webapp.jpclases;

public class StringsJp {

	public StringsJp(){
		
	}
	
	public String concatenateString(String a, String b) {
		return a + b;
	}

	public String getFirstCharacter(String a) {
		return String.valueOf(a.charAt(0));
	}

	public String getLastCharacter(String a) {
		String b = "";
		if (a.length() > 0) {
			b = String.valueOf(a.charAt(a.length() - 1));
		}
		return b;
	}
}
