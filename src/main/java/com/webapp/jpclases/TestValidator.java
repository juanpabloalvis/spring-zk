package com.webapp.jpclases;

import com.webapp.controller.zk.utils.User;
import org.apache.commons.lang3.Validate; 

public class TestValidator {
	
	private User user = null;
	
	public TestValidator() {
		user = new User(2L, "Juan", "123", "2");
	}
	
	public static void validateUser(User user){
		Validate.notNull(user, "The user is null");
		Validate.notNull(user.getName(), "The user name is null");
		//new User(0, null, null, null);
	}
	
	public static void main(String args[]){
		User test = new User();
		//test = new User(2L, "Juan", "123", "2");
		validateUser(test);
		System.out.println("This object is ok");
	}
	

}
