package com.webapp.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/spring/")
public class ApplicationController {

   private String cadenaConInjections = "{\n" +
           "  nombre:{primerNombre:\"juan\", segundoNombre:\"Pablo\"},\n" +
           "  apellido:{primerApellido:\"Alvis\"}\n" +
           "}";
	//Run as http://localhost:8080/spring/Test
   @RequestMapping(value="/Test", method = RequestMethod.GET)
   public String welcome(ModelMap modelRef) {
      modelRef.addAttribute("msgArgument", "Maven Java Web Application Project: Success!");
       modelRef.addAttribute("damerd", "mierda");
      if (cadenaConInjections == null) {
         System.out.println("test");
      }

      if (cadenaConInjections.equals("damierd")) {
         System.out.println("anotherTest");
      }

      return "pages/index.jsp";
   }

   // Run as http://localhost:8080/spring/Print/juan%20pablo%20alvis
   @RequestMapping(value="/Print/{arg}", method = RequestMethod.GET)
   public String welcomeName(@PathVariable String arg, ModelMap model) {
      model.addAttribute("msgArgument", "Maven Java Web Application Project, input variable: " + arg);
      return "pages/index.jsp";
   }
}
