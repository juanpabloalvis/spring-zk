package com.webapp.controller.zk;

import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.GenericRichlet;
import org.zkoss.zul.*;

public class TestRichlet extends GenericRichlet {
	// Richlet//
	public void service(Page page) {

		final Window win = new Window("ZK Essentials", "normal", false);

		win.setWidth("450px");
		Vlayout vl = new Vlayout();
		vl.setParent(win);

		final Textbox txtbx = new Textbox();
		txtbx.setParent(vl);

		final Label lbl = new Label();
		lbl.setParent(vl);

		final Button helloBtn = new Button();
		helloBtn.setLabel("Boton hola");
		helloBtn.setParent(win);

		helloBtn.addEventListener("onClick", new EventListener() {

			@Override
			public void onEvent(Event arg0) throws Exception {

				lbl.setValue(helloBtn.getLabel());
			}
		});

		final Button byeBtn = new Button();
		byeBtn.setLabel("Chao");
		byeBtn.setParent(win);

		byeBtn.addEventListener("onClick", new EventListener() {

			@Override
			public void onEvent(Event arg0) throws Exception {

				lbl.setValue(byeBtn.getLabel());
			}
		});

		txtbx.addEventListener("onChange", new EventListener() {
			@Override
			public void onEvent(Event arg0) throws Exception {

				lbl.setValue(txtbx.getValue());
			}
		});

		// manipulando la pagina "nestedcomponent.zul"
		//Window innerWin = (Window) win.getFellow("innerWin");

		win.setPage(page);
	}
}