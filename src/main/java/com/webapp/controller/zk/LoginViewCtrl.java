package com.webapp.controller.zk;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.webapp.controller.zk.utils.UserCredentialManager;

public class LoginViewCtrl extends GenericForwardComposer {

	/**
	 * com.webapp.controller.zk.utils.LoginViewCtrl
	 * @author jpalbis probando doble commit
	 */
	private static final long serialVersionUID = 1181721527001636869L;

	// Cuando se declaran las variables como privadas,
	// los compoenentes quedan auto enlazados con los componentes de los
	// archivos .zul
	private Textbox nameTxb;
	private Textbox passwordTxb;
	private Label mesgLbl;
	private Button confirmBtn;
	// Despues de esto, ya podemos acceder a los componentes.

	public void onClick$confirmBtn() {
		doLogin();
	};

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		// en caso de que personas que ya esten logueadas, debemos considerar un
		// mecanismo
		// antes de cargar la UI, esto se hace sobreescribiendo el metodo:
		// GenericForwardComposer.doAfterCompose(org.zkoss.zk.ui.Component)
		// el cual será llamado cuando los componentes esten enlazados. Así que
		// si sobreercribimos,
		// este método, debemos asegurarnos de llamar al método super().
		// to be implemented, let’s check for a login
	}

	private void doLogin() {
		UserCredentialManager mgmt = UserCredentialManager.getInstance(Sessions.getCurrent());
		mgmt.login(nameTxb.getValue(), passwordTxb.getValue());
		if (mgmt.isAuthenticated()) {
			execution.sendRedirect("ejBorderLayout");
		} else {
			mesgLbl.setValue("Your User Name or Password is invalid!");
		}
	}
}
