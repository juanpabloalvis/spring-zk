package com.webapp.controller.zk;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/zk/")
@SessionAttributes("todoList")
public class TodoController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model) {
		// ListModelList<Todo> todoList = new ListModelList<Todo>();
		// todoList.add(new Todo("Wakeup early"));
		// todoList.add(new Todo("Booking a restaurant"));
		// todoList.add(new Todo("Visit Jobs' office"));
		// model.addAttribute(todoList);

		return "forward:list";
	}

	// @RequestMapping(value = "/finish", method = RequestMethod.POST)
	// public String finish(@ZKVariable("self.value") Todo todo) {
	// todo.setDone(!todo.isDone());
	// return ZKModelAndView.SELF; // meaning the view is handled by ZK way.
	// }

	// Run as http://localhost:8080/zk/list
	@RequestMapping(value = "/list", method = { RequestMethod.GET, RequestMethod.POST })
	public String list() {
		return "zul/ajax.zul";
	}

	// Run as http://localhost:8080/zk/nestedcomponent
	@RequestMapping(value = "/nestedcomponent", method = { RequestMethod.GET, RequestMethod.POST })
	public String callNestedComponent() {
		return "zul/nestedcomponent.zul";
	}

	// Run as http://localhost:8080/zk/controllerEvents
	@RequestMapping(value = "/controllerEvents", method = { RequestMethod.GET, RequestMethod.POST })
	public String callControllerEvents() {
		return "zul/controllerEvents.zul";
	}

	// Run as http://localhost:8080/zk/ejBorderLayout
	@RequestMapping(value = "/ejBorderLayout", method = { RequestMethod.GET, RequestMethod.POST })
	public String callMainZkPage() {
		return "zul/ejBorderLayout.zul";
	}

	// Run as http://localhost:8080/zk/login
	@RequestMapping(value = "/login", method = { RequestMethod.GET, RequestMethod.POST })
	public String callLogin() {
		return "zul/login.zul";
	}
	/*
	 * @RequestMapping(value = "/add", method = RequestMethod.POST) public
	 * String add(@ModelAttribute("todoList") ListModelList<Todo> todoList,
	 * 
	 * @ZKSelector("#message") String message) { todoList.add(new
	 * Todo(message)); return "forward:list"; }
	 *
	 * @RequestMapping(value = "/edit", method = RequestMethod.POST) public
	 * String edit(@ModelAttribute("todoList") ListModelList<Todo>
	 * todoList, @ZKSelector("#status") Label status,
	 * 
	 * @ZKSelector("#message") Textbox message, @ZKSelector("#submit") Button
	 * submit) {
	 * 
	 * // get the current selected item Todo editTodo =
	 * todoList.getSelection().iterator().next();
	 * message.setValue(editTodo.getMessage());
	 * 
	 * status.setValue("Edit:");
	 * 
	 * submit.setLabel("Update");
	 * submit.setClientDataAttribute("springmvc-action", "update"); // change //
	 * to // mapping // to // '/update'
	 * 
	 * return ZKModelAndView.SELF; }
	 * 
	 * @RequestMapping(value = "/update", method = RequestMethod.POST) public
	 * String update(@ModelAttribute("todoList") ListModelList<Todo>
	 * todoList, @ZKSelector("#status") Label status,
	 * 
	 * @ZKSelector("#message") String message, @ZKSelector("#submit") Button
	 * submit) {
	 * 
	 * Todo editTodo = todoList.getSelection().iterator().next();
	 * editTodo.setMessage(message);
	 * 
	 * status.setValue("Add:");
	 * 
	 * submit.setLabel("Add new Todo");
	 * submit.setClientDataAttribute("springmvc-action", "add");
	 * 
	 * // save to model todoList.update(editTodo);
	 * 
	 * return "forward:list"; }
	 */
}