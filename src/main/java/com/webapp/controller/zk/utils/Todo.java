package com.webapp.controller.zk.utils;

public class Todo {
	private String message;
    private boolean done;
    
    public Todo() {
		// TODO Auto-generated constructor stub
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Todo(String message) {
		super();
		this.message = message;
	}
    
	
	
    
}
